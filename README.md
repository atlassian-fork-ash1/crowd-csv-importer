## Crowd CSV importer

### Introduction

The Crowd CSV Importer was written for quickly and easily importing users and groups into an Atlassian Crowd instance without requiring a UI. You just provide it with some CSV files and it will perform the imports for you.

### Building

    mvn package

### Running

For version X.Y:

    java -jar target/crowd-csv-importer-X.Y-SNAPSHOT.jar --help

This will show usage instructions.
