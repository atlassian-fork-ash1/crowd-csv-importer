package com.atlassian.crowd.csvimporter.callables;

import java.util.concurrent.Callable;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.integration.rest.entity.UserEntity;
import com.atlassian.crowd.service.client.CrowdClient;

import com.google.common.annotations.VisibleForTesting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateUserCallable implements Callable<Boolean>
{
    private static final Logger log = LoggerFactory.getLogger(CreateUserCallable.class);

    private final CrowdClient crowdClient;
    private final UserEntity user;
    private final boolean passwordsAreEncrypted;

    public CreateUserCallable(CrowdClient crowdClient, UserEntity user, boolean passwordsAreEncrypted)
    {
        this.crowdClient = crowdClient;
        this.user = user;
        this.passwordsAreEncrypted = passwordsAreEncrypted;
    }

    @Override
    public Boolean call()
    {
        try
        {
            crowdClient.addUser(user, new PasswordCredential(user.getPassword().getValue(), passwordsAreEncrypted));
            log.info("Sucessfully created user: {} ({})", user.getDisplayName(), user.getName());
            return Boolean.TRUE;
        }
        catch (InvalidUserException e)
        {
            log.warn("Failed to create username '{}': user already exists.", user.getName());
        }
        catch (InvalidCredentialException e)
        {
            log.error("Failed to create username '{}': CSV is missing password.", user.getName());
        }
        catch (OperationFailedException | InvalidAuthenticationException | ApplicationPermissionException e)
        {
            log.error("User not created. Unexpected error code while creating user: " + user.getName(), e);
        }

        return Boolean.FALSE;
    }
}
