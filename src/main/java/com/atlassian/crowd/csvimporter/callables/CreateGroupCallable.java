package com.atlassian.crowd.csvimporter.callables;

import java.util.concurrent.Callable;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.integration.rest.entity.GroupEntity;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.service.client.CrowdClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateGroupCallable implements Callable<Boolean>
{
    private static final Logger log = LoggerFactory.getLogger(CreateGroupCallable.class);

    private final CrowdClient crowdClient;
    private final String groupName;

    public CreateGroupCallable(CrowdClient crowdClient, String groupName)
    {
        this.crowdClient = crowdClient;
        this.groupName = groupName;
    }

    @Override
    public Boolean call()
    {
        try
        {
            crowdClient.addGroup(new GroupEntity(groupName, groupName + ": created by the CSV Importer",
                GroupType.GROUP, true));
            log.info("Created group: {}", groupName);
            return Boolean.TRUE;
        }
        catch (OperationFailedException | InvalidAuthenticationException | ApplicationPermissionException | InvalidGroupException e)
        {
            log.error("Could not create the group: " + groupName, e);
        }

        return Boolean.FALSE;
    }
}
