package com.atlassian.crowd.csvimporter;

/**
 * Make an enum for the possible exit codes
 */
public enum AppExitCode
{
    SUCCESS, ARGUMENTS_UNPARSIBLE, REQUIRED_ARGUMENTS_MISSING, GROUPS_FILE_NOT_EXIST, USERS_FILE_NOT_EXIST, LINE_READING_ERROR, CSV_PARSING_FAILURE;

    public void exit()
    {
        System.exit(ordinal());
    }
}
