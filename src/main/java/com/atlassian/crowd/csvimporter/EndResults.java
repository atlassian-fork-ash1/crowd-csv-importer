package com.atlassian.crowd.csvimporter;

public class EndResults
{
    private int groupsCreated, usersCreated, groupMembershipsCreated;
    private int totalUsers, totalGroups, totalGroupMemberships, totalParsedGroupMemberships;

    public int getGroupsCreated()
    {
        return groupsCreated;
    }

    public void setGroupsCreated(int groupsCreated)
    {
        this.groupsCreated = groupsCreated;
    }

    public int getUsersCreated()
    {
        return usersCreated;
    }

    public void setUsersCreated(int usersCreated)
    {
        this.usersCreated = usersCreated;
    }

    public int getGroupMembershipsCreated()
    {
        return groupMembershipsCreated;
    }

    public void setGroupMembershipsCreated(int groupMembershipsCreated)
    {
        this.groupMembershipsCreated = groupMembershipsCreated;
    }

    public int getTotalUsers()
    {
        return totalUsers;
    }

    public void setTotalUsers(int totalUsers)
    {
        this.totalUsers = totalUsers;
    }

    public int getTotalGroups()
    {
        return totalGroups;
    }

    public void setTotalGroups(int totalGroups)
    {
        this.totalGroups = totalGroups;
    }

    public int getTotalLegalGroupMemberships()
    {
        return totalGroupMemberships;
    }

    public void setTotalGroupMemberships(int totalGroupMemberships)
    {
        this.totalGroupMemberships = totalGroupMemberships;
    }

    public int getTotalParsedGroupMemberships()
    {
        return totalParsedGroupMemberships;
    }

    public void setTotalParsedGroupMemberships(int totalParsedGroupMemberships)
    {
        this.totalParsedGroupMemberships = totalParsedGroupMemberships;
    }
}
