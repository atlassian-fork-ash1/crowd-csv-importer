package com.atlassian.crowd.csvimporter.parsers;

import com.atlassian.crowd.csvimporter.parsers.exceptions.CsvParsingException;

public interface CsvMapper<T>
{
    String[] expectedHeader();

    boolean headerMatches(String[] headerLine);

    T generate(String[] csvLine) throws CsvParsingException;
}
