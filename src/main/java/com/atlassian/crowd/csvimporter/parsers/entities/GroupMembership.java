package com.atlassian.crowd.csvimporter.parsers.entities;

public class GroupMembership
{
    private final String username, group;

    public GroupMembership(String username, String group)
    {
        this.username = username;
        this.group = group;
    }

    @Override
    public String toString()
    {
        return "GroupMembership{" +
            "username='" + username + '\'' +
            ", group='" + group + '\'' +
            '}';
    }

    public String getUsername()
    {
        return username;
    }

    public String getGroup()
    {
        return group;
    }
}
