package com.atlassian.crowd.csvimporter.parsers;

import java.util.Arrays;

import com.atlassian.crowd.csvimporter.parsers.entities.GroupMembership;
import com.atlassian.crowd.csvimporter.parsers.exceptions.CsvParsingException;

public class GroupMapper extends AbstractCsvMapper<GroupMembership>
{
    @Override
    public String[] expectedHeader()
    {
        return new String[]{
            "Username", "Groupname"
        };
    }

    @Override
    public GroupMembership generate(String[] csvLine) throws CsvParsingException
    {
        if (csvLine.length != 2)
        {
            throw new CsvParsingException("Expecting exactly 2 columns but received " + csvLine.length);
        }

        return new GroupMembership(csvLine[0], csvLine[1]);
    }
}
